Insular is a FLOSS fork of Island.
With Insular, you can:<ul>
<li>Isolate your Big Brother apps</li>
<li>Clone and run multiple accounts simutaniuosly</li>
<li>Freeze or archive apps and prevent any background behaviors</li>
<li>Unfreeze apps on-demand with home screen shortcuts</li>
<li>Re-freeze marked apps with one tap</li>
<li>Hide apps</li>
<li>Selectively enable (or disable) VPN for different group of apps</li>
<li>Prohibit USB access to mitigate attacks with physical access</li></ul>
If your device is incompatible or not encrypted, you can skip this limitation manually. Please refer to <a href="https://forum.xda-developers.com/android/-t3366295">the XDA post</a> for details.
To uninstall and remove Insular completely, please first "Destroy Insular" in Settings - Setup - Click the recycle-bin icon besides Insular. If you have already uninstalled Insular app, please "Remove work profile" in your device "Settings - Accounts".

<b>PERMISSIONS</b><ul>
<li>DEVICE-ADMIN: Device administrator privilege is required to create the Insular space (work profile), which serves as the fundamental functionality of Insular. It will be explicitly requested for your consent.</li>
<li>PACKAGE_USAGE_STATS: Required to correctly recognize the running state of apps. It will be explicitly requested for your consent.</li></ul>
We will never collect any data and Insular have no Internet permission. Please read our privacy policy for more details.

<b>Differences from Island</b><ul>
<li>all blobs (gms, crashlytics, etc) are removed to comply with F-droid's policy</li>
<li>Internet access of this app is removed because we just don't need it</li>
All features are preserved and will be synced upon Island's updates
